import requests

def get_grid_levels(interval_numbers):
    url = "https://api.binance.com/api/v3/klines"
    params = {
        'symbol' : 'BTCUSDT',
        'interval' : '1h',
        'limit' : 200
    }
    response = requests.get(url, params=params)
    data = response.json()
    high_price = 0
    low_price = float(data[0][3])
    levels_buy = []
    levels_sell = []
    
    for bar in data:
        
        high_price = float(bar[2]) if float(bar[2]) > high_price else high_price
        low_price = float(bar[3]) if float(bar[3]) < low_price else low_price
        

    mid_price = round((high_price+low_price)/2,2)
    interval = (mid_price-low_price)/interval_numbers
    levels_buy.append(low_price)
    levels_sell.append(high_price)
    for i in range(interval_numbers-1):
        levels_buy.append(round(levels_buy[-1]+interval,2))
        levels_sell.append(round(levels_sell[-1] - interval,2))
    

    levels = {"buy" : levels_buy,
              "sell" : levels_sell,
              "mid" : mid_price}
    return levels

def generate_lines_for_pine(levels):
    print(f"levels_buy = array.new_float({len(levels['buy'])})")
    i = 0
    for buy_level in levels['buy']:
        print(f"array.set(levels_buy, {i}, {buy_level})")
        i += 1


    print(f"level_mid = {levels['mid']}")
    # print(f"array.set(level_mid, 0, {levels['mid']})")

    print(f"levels_sell = array.new_float({len(levels['sell'])})")
    i = 0
    for sell_level in levels['sell']:
        print(f"array.set(levels_sell, {i}, {sell_level})")
        i += 1
       

if __name__ == "__main__":
    levels = get_grid_levels(15)
    generate_lines_for_pine(levels)